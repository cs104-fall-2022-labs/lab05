def is_prime_number(n):
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


count = 0
number = 2
while count < 20:
    if is_prime_number(number):
        print(number)
        count += 1
    number += 1
