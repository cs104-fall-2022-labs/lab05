sum = 0
user_input = int(input("Please enter a number: "))
while not user_input < 0:
    sum += user_input
    user_input = int(input("Please enter a number: "))

print("Sum of all non-negative numbers previously given is ", sum)
