def is_palindrome(word):
    for i in range(len(word) // 2):
        if word[i].upper() != word[-1 * (i + 1)].upper():
            return False
    return True


print(is_palindrome("dogGod"))  # Expect True
print(is_palindrome("doGood"))  # Expect False
print(is_palindrome("RaceCar"))  # Expect True
print(is_palindrome("Madam"))  # Expect True
