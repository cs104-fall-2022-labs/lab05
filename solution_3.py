import random

computers_number = random.randint(1, 100)
number_of_guesses = 0
users_guess = 0
while users_guess != computers_number:
    users_guess = int(input("Guess a number: "))
    number_of_guesses += 1
    if users_guess > computers_number:
        print("Too high!")
    elif users_guess < computers_number:
        print("Too low!")

print("computers_number", computers_number)
print("number_of_guesses", number_of_guesses)
